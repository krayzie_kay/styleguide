/**
 * Created by rosia on 8/13/17.
 */
import React, { Component } from 'react';
import ToolTip from 'react-portal-tooltip';
import PropTypes from 'prop-types';

class CustomPopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showStatus: false,
            style: {
                style: {

                },
                arrowStyle: {
                    display: 'none',
                }
            }
        }
    }

    componentWillMount() {
        this.setState({
            name: this.props.name,
            div: this.props.div
        });
    }

    componentWillReceiveProps(props) {
        this.setState({
            name: props.name,
            div: props.div
        });
    }

    onBlur = (e) => {
        if (!this.focusInCurrentTarget(e)) {
            this.setState({showStatus: false});
        }
    };

    /**
     * @param relatedTarget
     * @param currentTarget
     * @returns {boolean}
     */
    focusInCurrentTarget = ({ relatedTarget, currentTarget }) => {
        if (relatedTarget === null) {
            return false;
        }

        let node = relatedTarget.parentNode;

        while (node !== null) {
            if (node === currentTarget) {
                return true;
            }
            node = node.parentNode;
        }

        return false;
    };


    render(){
        const { name, div} = this.state;
        const idName = name.replace(/ /g,'');
        return (
            <span>
                <span className="material-icons" id={`${idName}`} onClick={() => this.setState({showStatus: !this.state.showStatus})}>{name}</span>
                {
                    this.state.showStatus ?
                        <ToolTip active={true} position="bottom" arrow="right" parent={`#${idName}`} style={this.state.style}>
                            <div className="popup-wrapper" >
                                {div}
                            </div>
                        </ToolTip>
                        : null

                }
            </span>
        );
    }
}
CustomPopUp.PropTypes ={
    name: PropTypes.string,
    div: PropTypes.object
};

export default CustomPopUp;
