/**
 * Created by rosia on 8/10/17.
 */
/**
 * Created by rosia on 8/9/17.
 */
import React, { Component } from 'react';
import { DateRange } from 'react-date-range';
import PropTypes from 'prop-types';
import { Button } from 'react-mdc-web';
import moment from 'moment';

class CustomDateRangePicker extends Component {
    constructor(props) {
        super(props);
        this.state={
            startDate: '',
            endDate:'',
            dataToggleCounter: 0,
            toggleView: false
        }
    }

    componentDidMount() {
        this.setState({
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            selectedDates: this.props.onChange
        })
    }

    componentWillReceiveProps(props) {
        this.setState({
            startDate: props.startDate,
            endDate: props.endDate,
            selectedDates: props.onChange
        })
    }

    handleSelect = (range) => {
        this.setState({
            startDate: range.startDate,
            endDate: range.endDate,
            toggleView: this.state.dataToggleCounter % 2  === 0,
            dataToggleCounter: this.state.dataToggleCounter + 1
        }, () => {
            if(!this.state.toggleView) {
                this.state.selectedDates(range.startDate, range.endDate);
            }
        });
    };

    onBlur = (e) => {
        if (!this.focusInCurrentTarget(e)) {
            this.setState({toggleView: false});
        }
    };

    /**
     * Function checks if the blurred target is inside the daterangepicker div or not
     * @param relatedTarget
     * @param currentTarget
     * @returns {boolean}
     */
    focusInCurrentTarget = ({ relatedTarget, currentTarget }) => {
        if (relatedTarget === null) {
            return false;
        }

        let node = relatedTarget.parentNode;

        while (node !== null) {
            if (node === currentTarget) {
                return true;
            }
            node = node.parentNode;
        }

        return false;
    };

    render() {
        return (
            <div className="date-range-picker-wrapper" tabIndex="0" onBlur={this.onBlur}>
                <span className="calendar-icon" onClick={() => this.setState({toggleView: !this.state.toggleView})}>
                    <i className={this.state.toggleView ? 'material-icons fixed-calendar-active':'material-icons fixed-calendar'}>event_note</i>
                </span>
                <div className={this.state.toggleView ? 'date-range-picker-view-toggle-active' : 'date-range-picker-view-toggle'}
                     onClick={() => this.setState({toggleView: !this.state.toggleView})}>
                    {
                        this.state.startDate || this.state.endDate ?
                            <span id="date-range-picker-id">
                                {this.state.startDate.format('Do MMM YYYY')}  -  {this.state.endDate.format('Do MMM YYYY')}
                            </span> :
                            <span id="date-range-picker-id">
                                Select Date
                            </span>
                    }
                    <i className="material-icons">arrow_drop_down</i>
                </div>
                {
                    this.state.toggleView ?
                        <div>
                            <DateRange
                                onChange={this.handleSelect}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                            />
                        </div>
                        : null
                }
            </div>
        );
    }
}
CustomDateRangePicker.defaultProps = {
    list: [],
    name: ''
};

CustomDateRangePicker.PropTypes = {
    onChange: PropTypes.func,
    list: PropTypes.array,
    name: PropTypes.string,
    startDate: PropTypes.object,
    endDate: PropTypes.object
};


export default CustomDateRangePicker;

