/**
 * Created by rosia on 8/15/17.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _range from 'lodash/range';
import CustomSelect from './../Select/CustomSelect';
import { Icon } from 'react-mdc-web';
// ...required at end if the page value cannot find max page by 3
const pagesInEachHalf = 3;
const totalNumberOfPages = 2;
class Pagination extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageSize: props.pageSize,
            currentPage: 1,
            totalItems: props.totalItems,
            numberOfPages: 1,
            isLastActive: true,
            isNextActive: true,
            isFirstActive: true,
            isPreviousActive: true
        };
    }
    componentDidMount() {
        this._setPropsToState();
    }
    componentWillReceiveProps(nextProps) {
        this._setPropsToState();
    }
    handlePageChange = (pageNumber) => {
        this.setState({currentPage: pageNumber}, () => {this.props.onPageSelect(pageNumber,this.state.pageSize)})
    };
    /** return if lower limit can be found */
    couldFindLowerLimit = (currentPage, totalPage) => {
        return !(currentPage > pagesInEachHalf+1);
    };
    /** return if upper limit can be found */
    couldFindUpperLimit = (currentPage, totalPage) => {
        return totalPage - currentPage <= pagesInEachHalf
    };
    /**
     * update the pageSize change
     */
    handlePageSizeChange = (event) => {
        this.setState({pageSize: event.target.value, currentPage:1}, () => {this.props.onPageSelect(this.state.currentPage, this.state.pageSize)});
    };
    returnArrayForPages = (lowerLimitReached, upperLimitReached, totalNumberOfPages, currentPage) => {
        let startIndex = 1;
        let endIndex = totalNumberOfPages;
        let array = [];
        if (!lowerLimitReached) {
            startIndex = currentPage - pagesInEachHalf;
        }
        if (!upperLimitReached) {
            endIndex = currentPage + pagesInEachHalf;
        }
        for (var count = startIndex; count <= endIndex; count++) {
            array.push(count);
        }
        return array;
    };
    render() {
        const {currentPage, numberOfPages, pageSize} = this.state;
        const {orientation} = this.props;
        const lowerLimitReached = this.couldFindLowerLimit(currentPage, numberOfPages);
        const upperLimitReached = this.couldFindUpperLimit(currentPage, numberOfPages);
        const arrayForPages = this.returnArrayForPages(lowerLimitReached, upperLimitReached, numberOfPages, currentPage);
        return (
            <div className="pagination-wrapper">
                <CustomSelect
                    list = { [
                        {label: '50', value: 50},
                        {label: '100', value: 100},
                        {label: '150', value: 150}
                    ]}
                    name="pagination-select"
                    className="pagination-dropdown"
                    defaultValue="50"
                    orientation={orientation}
                    onChange={this.handlePageSizeChange}
                />

                {/*<select className="pagination-total-pages" value={pageSize} onChange={this.handlePageSizeChange}>*/}
                    {/*<option value="50">50</option>*/}
                    {/*<option value="100">100</option>*/}
                    {/*<option value="150">150</option>*/}
                {/*</select>*/}
                <span onClick={this.state.isFirstActive ? () => this._onClickFirst() : null}><Icon name="skip_previous" /></span>
                <span onClick={this.state.isPreviousActive ? () => this._onClickPrevious() : null}><Icon name="keyboard_arrow_left" /></span>
                {!lowerLimitReached && <span>...</span>}
                <span>
                    {arrayForPages.map((page) => <span className={ page !== this.state.currentPage ? 'pagination-page' : 'pagination-page pagination-page-active'} onClick={page !== this.state.currentPage ? () => this.handlePageChange(page):null}>{page}</span>)}
                </span>
                {!upperLimitReached && <span>...</span>}
                <span onClick={this.state.isNextActive ? () => this._onClickNext() : null}><Icon name="keyboard_arrow_right" /></span>
                <span onClick={this.state.isLastActive ? () => this._onClickLast() : null}><Icon name="skip_next" /></span>
            </div>
        );
    }
    /**
     * @private
     * get total number of pages.
     */
    _setPropsToState = () => {
        let totalNumberOfPages = Math.ceil(this.props.totalItems / this.props.pageSize);
        this.setState({
            numberOfPages: totalNumberOfPages
        }, () => {
            this._setActiveButtons();
        })
    }
    _setActiveButtons = () => {
        if (this.state.currentPage === 1) {
            this.setState({isFirstActive: false, isPreviousActive: false, isLastActive: true, isNextActive: true});
        } else if (this.state.currentPage === this.state.numberOfPages) {
            this.setState({isLastActive: false, isNextActive: false, isFirstActive: true, isPreviousActive: true});
        } else {
            this.setState({isLastActive: true, isNextActive: true, isFirstActive: true, isPreviousActive: true})
        }
    }
    _onClickPage = (page) => {
        this.setState({currentPage: page}, () => {
            this._setActiveButtons();
            this.props.onPageSelect(page,this.state.pageSize);
        })
    }
    _onClickNext = () => {
        this.setState({currentPage: this.state.currentPage + 1}, () => {
            this._setActiveButtons();
            this.props.onPageSelect(this.state.currentPage, this.state.pageSize);
        });
    }
    _onClickPrevious = () => {
        this.setState({currentPage: this.state.currentPage - 1}, () => {
            this._setActiveButtons();
            this.props.onPageSelect(this.state.currentPage, this.state.pageSize);
        });
    }
    _onClickFirst = () => {
        this.setState({currentPage: 1}, () => {
            this._setActiveButtons();
            this.props.onPageSelect(this.state.currentPage, this.state.pageSize);
        })
    }
    _onClickLast = () => {
        this.setState({currentPage: this.state.numberOfPages}, () => {
            this._setActiveButtons();
            this.props.onPageSelect(this.state.currentPage, this.state.pageSize);
        });
    }
}

Pagination.defaultProps = {
    pageLimit: 50,
    orientation: 'bottom'
};
Pagination.PropTypes = {
    pageSize: PropTypes.number.isRequired,
    totalItems: PropTypes.number.isRequired,
    onPageSelect: PropTypes.func.isRequired,
    orientation: PropTypes.string
};
export default Pagination;