/**
 * Created by rosia on 8/13/17.
 */
import React, { Component } from 'react';
import ToolTip from 'react-portal-tooltip';
import PropTypes from 'prop-types';

class CustomPopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showStatus: false,
            style: {
                style: {
                    backgroundColor: '#FFFEF2'
                },
                arrowStyle: {
                    color: '#fff',
                }
            }
        }
    }

    componentWillMount() {
        this.setState({
            heading: this.props.heading,
            date: this.props.date,
            body: this.props.body,
            name: this.props.name,
            position: this.props.position ? this.props.position : 'bottom',
            arrowPosition: this.props.arrowPosition ? this.props.arrowPosition : 'center',
        });
    }

    componentWillReceiveProps(props) {
        this.setState({
            heading: props.heading,
            date: props.date,
            body: props.body,
            name: props.name,
            position: props.position ? props.position : 'bottom',
            arrowPosition: props.arrowPosition ? props.arrowPosition : 'center',
        });
    }

    render(){
        /**
         * heading -> Header of the popup
         * date -> Date of the popup
         * body -> The actual message of the popup
         * name -> The text on whose click the popup will be displayed
         * position -> Position of the arrow
         */
        const {heading, date, body, name, position, arrowPosition} = this.state;
        const idName = name.replace(/ /g,'');
        return (
            <span>
                <span className="material-icons" id={`${idName}`} onClick={() => this.setState({showStatus: true})}>{name}</span>
                {
                    this.state.showStatus ?
                        <ToolTip active={true} position={position} arrow={arrowPosition} parent={`#${idName}`} style={this.state.style}>
                            <div className="popup-wrapper">
                                <div className="popup-header">
                                    {heading}
                                    <i className="material-icons" onClick={() => this.setState({showStatus: false})}>clear</i>
                                </div>
                                <div className="popup-date">{date}</div>
                                <div className="popup-body">{body}</div>
                            </div>
                        </ToolTip>
                        :null
                }
            </span>
        );
    }
}
CustomPopUp.PropTypes ={
    heading: PropTypes.string,
    date: PropTypes.string,
    body: PropTypes.string,
    name: PropTypes.string,
    position: PropTypes.string,
    arrowPosition: PropTypes.string
};

export default CustomPopUp;
