/**
 * Created by rosia on 8/13/17.
 */
import React, { Component } from 'react';
import ToolTip from 'react-portal-tooltip';
import PropTypes from  'prop-types';

class CustomToolTip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showStatus: false,
            style: {
                style: {
                    backgroundColor: '#616161',
                    opacity: 0.9,
                    color: '#fff',
                    textAlign: 'center',
                    borderRadius: 2,
                    padding: '0 8px',
                    zIndex: 1,
                    lineHeight: '22px',
                    fontSize: 10,
                    transition: 'opacity 0.3s easeout',
                },
                arrowStyle: {
                    display: 'none'
                }
            }
        }
    }

    render(){
        const {active, icon, tip} = this.props;
        return (
            <span className="custom-tooltip">
                <i className={active ? 'material-icons active' : 'material-icons'} id={`${icon}Icon`}
                    onMouseOver={() => this.setState({showStatus: true})}
                    onMouseOut={() => this.setState({showStatus: false})}>{icon}</i>
                <ToolTip active={this.state.showStatus} position="bottom" arrow="center" parent={`#${icon}Icon`} style={this.state.style}>
                    <div className="tooltip-text">
                        {tip}
                    </div>
                </ToolTip>
            </span>
        );
    }
}

CustomToolTip.PropTypes = {
    active: PropTypes.bool,
    icon: PropTypes.string,
    tip: PropTypes.string
}

export default CustomToolTip;