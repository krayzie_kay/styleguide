/**
 * Created by rosia on 8/9/17.
 */
import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import CustomDateRangePicker  from './DateRangePicker/DateRangePicker';
import InputRange from 'react-input-range';
import '../node_modules/react-input-range/lib/css/index.css';
import moment from 'moment';
import CustomToolTip from './ToolTip/ToolTip';
import Popup from './Popup/Popup';
import CustomPopup from './CustomPopup/CustomPopup';
import Collapsible from 'react-collapsible';
import Pagination from './Pagination/pagination';
import CustomSelect from './Select/CustomSelect';
import {
    Button, Drawer, DrawerHeader, DrawerHeaderContent, DrawerContent, DrawerSpacer, Navigation, Icon, LinearProgress,
    Fab, Grid, Cell, FormField, Textfield, Radio, Checkbox,
    Switch, Dialog, DialogHeader, DialogTitle, DialogBody, DialogFooter, Tabbar, Tab
} from 'react-mdc-web';

class Template extends Component {
    constructor(props) {
        super(props);
        this.state={
            selectedDate: moment(),
            rangeValue: {min: 20, max:50},
            leftDrawer: false,
            rightDrawer: false,
            modalOpen: false,
            activeTab: 1,
            showToolTip1: false,
            showToolTip2: false
        }
    }

    onChange = (e) => {
        console.log("Here is the value", e.target.value);
    };

    selectedDates = (startDate, endDate) => {
        console.log("Date selected from datepicker is" ,startDate.format('Do MMM YYYY'), endDate.format('Do MMM YYYY'));
    };

    paginationSelect =(pageLimit, value) =>{
        console.log("Pagination values : " , pageLimit, value);
    };

    render() {
        return (
            <div className="app">

                <div className="nav-bar">
                    <h1>
                        <Icon name="menu" onClick={() => {
                            this.setState({leftDrawer: !this.state.leftDrawer})
                        }}/>
                        <span className="logo-font">Rosia <strong>B</strong></span>
                    </h1>
                    <span className="nav-right-item">
                        <Icon name="exit_to_app"/>
                    </span>
                    <span className="nav-right-item">
                        <Icon name="menu"/>
                    </span>

                </div>

                <div className="content">
                    {/*<Drawer permanent>*/}
                    {
                        this.state.leftDrawer ?
                            <Drawer permanent>
                                <DrawerSpacer>
                                    Directions
                                </DrawerSpacer>
                                <Navigation>
                                    <a href='#' selected><Icon name='directions_bus'/>Bus</a>
                                    <a href='#'><Icon name='directions_railway'/>Railway</a>
                                    <a href='#'><Icon name='directions_bike'/>Bike</a>
                                </Navigation>
                            </Drawer> : null
                    }
                    <div className="content-body ">

                        {/*Headers and icons*/}
                        <Grid>
                            <Cell col={6}>
                                <div className="card">
                                    <p>
                                        <h1>Regular 24sp</h1>
                                    </p>
                                    <p>
                                        <h2>Medium 2sp</h2>
                                    </p>
                                    <p>
                                        <h3>Regular 16sp</h3>
                                    </p>
                                    <p>
                                        <h4>Medium 13sp</h4>
                                    </p>
                                    <p>
                                        <h5>Regular 13sp</h5>
                                    </p>
                                    <p>
                                        <h6>Regular 12sp</h6>
                                    </p>
                                    <p>
                                        <span className="h7">Medium(ALL caps) 14sp</span>
                                    </p>
                                </div>
                            </Cell>
                            <Cell col={6}>
                                <div className="card">
                                    <p><h5>These icons are inactive:</h5></p>
                                    <CustomToolTip icon="search" tip="Search Icon"/>
                                    <CustomToolTip icon="arrow_back" tip="Arrow Icon"/>
                                    <CustomToolTip icon="arrow_forward" tip="Forward Icon"/>
                                    <CustomToolTip icon="arrow_downward" tip="Downward Icon"/>
                                    <CustomToolTip icon="arrow_upward" tip="Upward Icon"/>
                                    <CustomToolTip icon="keyboard_arrow_up" tip="Up Icon"/>
                                    <CustomToolTip icon="keyboard_arrow_down" tip="Down Icon"/>
                                    <CustomToolTip icon="keyboard_arrow_left" tip="Left Icon"/>
                                    <CustomToolTip icon="keyboard_arrow_right" tip="Right Icon"/>
                                    <CustomToolTip icon="arrow_drop_down" tip="DropDown Icon"/>
                                    <CustomToolTip icon="arrow_drop_up" tip="DropUp Icon"/>
                                </div>

                                <div className="card">
                                    <p><h5>These icons are active:</h5></p>
                                    <CustomToolTip active={true} icon="search" tip="Search Icon"/>
                                    <CustomToolTip active={true} icon="arrow_back" tip="Arrow Icon"/>
                                    <CustomToolTip active={true} icon="arrow_forward" tip="Forward Icon"/>
                                    <CustomToolTip active={true} icon="arrow_downward" tip="Downward Icon"/>
                                    <CustomToolTip active={true} icon="arrow_upward" tip="Upward Icon"/>
                                    <CustomToolTip active={true} icon="keyboard_arrow_up" tip="Up Icon"/>
                                    <CustomToolTip active={true} icon="keyboard_arrow_down" tip="Down Icon"/>
                                    <CustomToolTip active={true} icon="keyboard_arrow_left" tip="Left Icon"/>
                                    <CustomToolTip active={true} icon="keyboard_arrow_right" tip="Right Icon"/>
                                    <CustomToolTip active={true} icon="arrow_drop_down" tip="DropDown Icon"/>
                                    <CustomToolTip active={true} icon="arrow_drop_up" tip="DropUp Icon"/>

                                </div>

                                <div className="card">
                                    <Popup
                                        heading="Note by Sushan Neupane"
                                        date="12 Jul 2017"
                                        body="Requested for extension of credit"
                                        name="search"
                                    />
                                    <Popup
                                        heading="Note by Megan Fox"
                                        date="14 Aug 2017"
                                        body="Requested for extension of credit"
                                        name="menu"
                                        position="bottom"
                                        arrowPosition="right"
                                    />
                                    <CustomPopup
                                        name="view_column"
                                        div={(
                                            <div>
                                                <div className="">
                                                    Heading
                                                </div>
                                                <hr/>
                                                <div>
                                                    <ul>
                                                        <li>First</li>
                                                        <li>Second</li>
                                                        <li>Third</li>
                                                    </ul>
                                                </div>
                                            </div>

                                        )}
                                    />
                                </div>

                            </Cell>
                        </Grid>

                        {/*Buttons*/}
                        <Grid>
                            <Cell col={6}>
                                <div className="card">
                                    <h3>Normal buttons:</h3>
                                    <p>
                                        <Button raised accent>Invoice</Button>
                                        <Button raised accent>Ok</Button>
                                        <Button className="cancel-btn" raised accent>Cancel</Button>
                                    </p>
                                    <hr/>
                                    <h3>Modal buttons: </h3>
                                    <p>
                                        <Button className="modal-btn" accent>Dispatch</Button>
                                        <Button className="modal-btn" accent>save</Button>
                                        <Button className="modal-btn cancel-btn" accent>Cancel</Button>
                                    </p>
                                    <p>
                                        <Fab onClick={ () => this.setState({rightDrawer: !this.state.rightDrawer})}>
                                            <Icon name='add'/>
                                        </Fab>
                                    </p>
                                </div>
                            </Cell>
                            <Cell col={6}>
                                <div className="card">
                                    <h3>Loaders</h3>
                                    <div>
                                        <LinearProgress accent indeterminate/>
                                    </div>
                                    <div>
                                        {/*<Preloader size="small"/>*/}
                                    </div>
                                </div>
                                <div className="card">
                                    <p>Example of normal badge:
                                        <span className="badge">3</span>
                                    </p>
                                    <p className="active">Example of active badge:
                                        <span className="badge">1232133</span>
                                    </p>
                                </div>
                            </Cell>
                        </Grid>


                        {/*Combination*/}
                        <Grid>
                            <Cell col={6}>
                                <div className="card">
                                    {/*Form Elements*/}
                                    <p>
                                        <h2>Form</h2>
                                    </p>
                                    <form action="#">
                                        <Textfield
                                            floatingLabel="Text Types"
                                            value={this.state.textType}
                                            onChange={this.onChange}
                                        />
                                        <br/>
                                        <Textfield
                                            floatingLabel="Disabled Sample"
                                            disabled
                                            onChange={this.onChange}
                                        />
                                        <br/>
                                        <Textfield
                                            floatingLabel="Password"
                                            type="password"
                                            value={this.state.password}
                                            onChange={this.onChange}
                                        />
                                        <br/>
                                        <Textfield
                                            floatingLabel="Email"
                                            type="email"
                                            value={this.state.email}
                                            onChange={this.onChange}
                                        />
                                        <br/>


                                        <CustomSelect
                                            list = { [
                                                {label: 'First', value: 'First'},
                                                {label: 'Second', value: 'Second'},
                                                {label: 'Third', value: 'Third'},
                                                {label: 'Fourth', value: 'Fourth'},
                                                ]}
                                            name="select1"
                                            onChange={ (e) => console.log("Select option: ", e.target.value)}
                                        />
                                        <br/>

                                        <CustomSelect
                                            list = { [
                                                {label: 'Fifth', value: 'Fifth'},
                                                {label: 'Sixth', value: 'Sixth'},
                                                {label: 'Seventh', value: 'Seventh'},
                                                {label: 'Eight', value: 'Eight'},
                                            ]}
                                            name="select2"
                                            orientation="top"
                                            onChange={ (value) => console.log("Select option: ", value)}
                                        />
                                        <br/>


                                        {/*Checkboxes*/}
                                        <div>
                                            <FormField id="labeled-checkbox">
                                                <Checkbox
                                                    onChange={this.onChange}
                                                />
                                                <label>Label1</label>
                                            </FormField>
                                            <FormField id="labeled-checkbox2">
                                                <Checkbox
                                                    onChange={this.onChange}
                                                />
                                                <label>Label2</label>
                                            </FormField>

                                        </div>

                                        <br/>

                                        {/*Radio*/}
                                        <div>
                                            <FormField id="labeled-male">
                                                <Radio name="gender"
                                                    onChange={this.onChange}
                                                />
                                                <label>Male</label>
                                            </FormField>
                                            <FormField id="labeled-female">
                                                <Radio name="gender"
                                                    onChange={this.onChange}
                                                />
                                                <label>Female</label>
                                            </FormField>
                                            <FormField id="labeled-others">
                                                <Radio name="gender"
                                                    onChange={this.onChange}
                                                />
                                                <label>Others</label>
                                            </FormField>
                                        </div>
                                        <br/>

                                        {/*Switch*/}
                                        <div>
                                            <FormField id="switch">
                                                <Switch onChange={this.onChange} />
                                                <label>On/Off</label>
                                            </FormField>
                                        </div>
                                        <br/>

                                        <div>
                                            <InputRange
                                                maxValue={100}
                                                minValue={0}
                                                value={this.state.rangeValue}
                                                onChange={value => this.setState({rangeValue: value})} />
                                        </div>
                                        <br/>
                                        <br/>

                                        <div>
                                            <p>
                                                <h2>Date Range Picker</h2>
                                                <CustomDateRangePicker
                                                    startDate={moment().add(5,'days')}
                                                    endDate={moment().add(9,'days')}
                                                    onChange={this.selectedDates}
                                                />
                                            </p>
                                        </div>
                                        <div>
                                            <Pagination pageSize={10} totalItems={100} onPageSelect={this.paginationSelect} />
                                        </div>
                                    </form>
                                </div>
                            </Cell>

                            <Cell col={6}>
                                <div className="card">
                                    {/*Modals*/}
                                    <Button accent raised onClick={ () => this.setState({modalOpen: true})}>Modal Sample</Button>
                                    <Dialog open={this.state.modalOpen} onClose={() => {this.setState({modalOpen:false})}}>
                                        <DialogHeader>
                                            <DialogTitle>Modify Data</DialogTitle>
                                        </DialogHeader>
                                        <DialogBody>
                                            <div>
                                                <Textfield
                                                    className="right-side-gap"
                                                    floatingLabel="Order Id"
                                                    value="132123"
                                                    disabled
                                                    onChange={this.onChange}
                                                />
                                                <Textfield
                                                    className="right-side-gap"
                                                    floatingLabel="Quantity"
                                                    onChange={this.onChange}
                                                />
                                                <Textfield
                                                    className="right-side-gap"
                                                    floatingLabel="Price"
                                                    value="3, 421.9"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                            <div>
                                                <Textfield
                                                    floatingLabel="Reason to Modify"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                        </DialogBody>
                                        <DialogFooter>
                                            <Button accent className="modal-btn cancel-btn" onClick={()=> { this.setState({modalOpen: false}) }}>Cancel</Button>
                                            <Button accent className="modal-btn" onClick={()=> { this.setState({modalOpen: false}) }}>Ok</Button>
                                        </DialogFooter>
                                    </Dialog>

                                    <br/>
                                    <br/>
                                    <br/>

                                    {/*Accordion*/}
                                    <div>
                                        <h3>
                                            Accordion
                                        </h3>
                                        <Collapsible trigger="Start here">
                                                <p className="default-horizontal-padding-24 default-vertical-padding-16">So this is an Accordion component that used the <a href="https://github.com/glennflanagan/react-collapsible">react-collapsible</a> component. How handy.</p>
                                        </Collapsible>
                                        <Collapsible trigger="Then here">
                                            <p className="default-horizontal-padding-24 default-vertical-padding-16">An Accordion is different to a Collapsible in the sense that only one "tray" will be open at any one time.</p>
                                        </Collapsible>
                                        <Collapsible trigger="And last here">
                                            <p className="default-horizontal-padding-24 default-vertical-padding-16">And this Accordion component is also completely repsonsive. Hurrah for mobile users!</p>
                                        </Collapsible>

                                    </div>

                                    <br/>

                                    {/*Simple tables*/}
                                    <h1>Normal Table</h1>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th data-field="id">Name</th>
                                                <th data-field="name">Item Name</th>
                                                <th data-field="price">Item Price</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>Alvin</td>
                                                <td>Eclair</td>
                                                <td>$0.87</td>
                                            </tr>
                                            <tr>
                                                <td>Alan</td>
                                                <td>Jellybean</td>
                                                <td>$3.76</td>
                                            </tr>
                                            <tr>
                                                <td>Jonathan</td>
                                                <td>Lollipop</td>
                                                <td>$7.00</td>
                                            </tr>
                                        </tbody>
                                    </table>


                                </div>
                            </Cell>

                        </Grid>

                        {/*Tabs*/}
                        <Grid>
                            <Cell col={12} className="card">
                                <Tabbar>
                                    <Tab
                                        active={this.state.activeTab===1}
                                        onClick={() => {this.setState({activeTab:1})}}>
                                        <div className="mdc-tab-header">
                                            <span>First Tab</span>
                                            <span className="badge">123</span>
                                        </div>
                                    </Tab>
                                    <Tab
                                        active={this.state.activeTab===2}
                                        onClick={() => {this.setState({activeTab:2})}}>
                                        <div className="mdc-tab-header">
                                            <span>Second Tab</span>
                                            <span className="badge">123456</span>
                                        </div>
                                    </Tab>
                                    <Tab
                                        active={this.state.activeTab===3}
                                        onClick={() => {this.setState({activeTab:3})}}>
                                        <div className="mdc-tab-header">
                                            <span>Third Tab</span>
                                            <span className="badge">123456789</span>
                                        </div>
                                    </Tab>
                                </Tabbar>
                                <div className="mdc-tab-body">
                                    {
                                        this.state.activeTab === 1 ?
                                            <div style={{padding: '16px 0'}}>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                            </div>
                                            :null
                                    }
                                    {
                                        this.state.activeTab === 2 ?
                                            <div style={{padding: '16px 0'}}>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                                            </div>
                                            :null
                                    }
                                    {
                                        this.state.activeTab === 3 ?
                                            <div style={{padding: '16px 0'}}>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                            </div>
                                            :null
                                    }
                                </div>
                            </Cell>
                        </Grid>

                        {/*Grid setup*/}
                        <div>
                            <Grid>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                                <Cell className="border-black" col={1}> Col-1 </Cell>
                            </Grid>
                            <Grid>
                                <Cell className="border-black" col={2}> Col-2 </Cell>
                                <Cell className="border-black" col={2}> Col-2 </Cell>
                                <Cell className="border-black" col={2}> Col-2 </Cell>
                                <Cell className="border-black" col={2}> Col-2 </Cell>
                                <Cell className="border-black" col={2}> Col-2 </Cell>
                                <Cell className="border-black" col={2}> Col-2 </Cell>
                            </Grid>
                            <Grid>
                                <Cell className="border-black" col={3}> Col-3 </Cell>
                                <Cell className="border-black" col={3}> Col-3 </Cell>
                                <Cell className="border-black" col={3}> Col-3 </Cell>
                                <Cell className="border-black" col={3}> Col-3 </Cell>
                            </Grid>
                            <Grid>
                                <Cell className="border-black" col={4}> Col-4 </Cell>
                                <Cell className="border-black" col={4}> Col-4 </Cell>
                                <Cell className="border-black" col={4}> Col-4 </Cell>
                            </Grid>
                            <Grid>
                                <Cell className="border-black" col={6}> Col-6 </Cell>
                                <Cell className="border-black" col={6}> Col-6 </Cell>
                            </Grid>
                            <Grid>
                                <Cell className="border-black" col={6} tablet={8}> 6 (tablet 8) </Cell>
                                <Cell className="border-black" col={4} tablet={6}> 4 (tablet 6) </Cell>
                                <Cell className="border-black" col={2} phone={4}> 2 (phone 4) </Cell>
                            </Grid>
                        </div>

                    </div>
                </div>


                <div dir="rtl">
                    <Drawer
                        className="right-drawer"
                        open={this.state.rightDrawer}
                        onClose={()=> { this.setState({rightDrawer: false}) }}>
                        <DrawerHeader>
                            <DrawerHeaderContent>
                                Directions
                            </DrawerHeaderContent>
                        </DrawerHeader>
                        <DrawerContent>
                            <Navigation>
                                <a href='#' selected><Icon name='directions_bus'/>Bus</a>
                                <a href='#'><Icon name='directions_railway'/>Railway</a>
                                <a href='#'><Icon name='directions_bike'/>Bike</a>
                            </Navigation>
                        </DrawerContent>
                    </Drawer>
                </div>
            </div>
        );
    }
}

export default Template;

